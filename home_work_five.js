const gameField = [
    ['x', 'o', ''],
    ['x', '', 'o'],
    ['', 'o', 'x']
    ];

function fieldOutput(result){
    if (result == "x") return "Крестики победили!"
    if (result == "o") return "Нолики победили!"
    if (result == '') return "Ничья"      
}

function gameResult(field){
    /* Все варианты побед */
    if (field[0][0] == field[1][1] && field[1][1] == field[2][2]) document.writeln(fieldOutput(field[0][0]));
    if (field[0][1] == field[1][1] && field[1][1] == field[2][1]) document.writeln(fieldOutput(field[0][1]));
    if (field[0][2] == field[1][1] && field[1][1] == field[2][0]) document.writeln(fieldOutput(field[0][2]));
    if (field[0][0] == field[1][0] && field[1][0] == field[2][0]) document.writeln(fieldOutput(field[0][0]));
    if (field[0][2] == field[1][2] && field[1][2] == field[2][2]) document.writeln(fieldOutput(field[0][2]));
    if (field[0][0] == field[0][1] && field[0][1] == field[0][2]) document.writeln(fieldOutput(field[0][0]));
    if (field[1][0] == field[1][1] && field[1][1] == field[1][2]) document.writeln(fieldOutput(field[1][0]));
    if (field[2][0] == field[2][1] && field[2][1] == field[2][2]) document.writeln(fieldOutput(field[2][0]));

    /* Ничья */
    if (field[0][0] == null && field[1][1] == null && field[2][2] == null) document.writeln(fieldOutput(field[0][0]));
    if (field[0][2] == null && field[1][1] == null && field[2][0] == null) document.writeln(fieldOutput(field[0][2]));
    if (field[0][0] == null && field[1][0] == null && field[2][0] == null) document.writeln(fieldOutput(field[0][0]));
    if (field[0][1] == null && field[1][1] == null && field[2][1] == null) document.writeln(fieldOutput(field[0][1]));
    if (field[0][2] == null && field[1][2] == null && field[2][2] == null) document.writeln(fieldOutput(field[0][2]));
    if (field[0][0] == null && field[0][1] == null && field[0][2] == null) document.writeln(fieldOutput(field[0][0]));
    if (field[1][0] == null && field[1][1] == null && field[1][2] == null) document.writeln(fieldOutput(field[1][0]));
    if (field[2][0] == null && field[2][1] == null && field[2][2] == null) document.writeln(fieldOutput(field[2][0]));
}

gameResult(gameField)
